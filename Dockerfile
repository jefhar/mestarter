FROM  phpdockerio/php74-cli:latest

RUN curl -sSo pubkey.gpg https://dl.yarnpkg.com/debian/pubkey.gpg \
    && apt-key add pubkey.gpg \
	&& echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list \
	&& apt-get update \
	&& apt-get -y --no-install-recommends install \
		npm \
		yarn \
	&& apt-get install -y --only-upgrade php7.4-cli php7.4-common \
	&& apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

# Install Laravel Envoy
RUN  composer self-update \
    && composer global require "laravel/envoy=~1.0" \
    && composer clear-cache
