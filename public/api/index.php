<?php

require __DIR__ . '/../../vendor/autoload.php';

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
$player = new MEApi\Player();
echo $player->json();
