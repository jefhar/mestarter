<?php
declare (strict_types=1);
namespace MEApi;

class PsychologicalProfile
{
    private const RUTHLESS = 'Ruthless';
    private const SOLE_SURVIVOR = 'Sole Survivor';
    private const WAR_HERO = 'War Hero';

    private $choices = [
        self::RUTHLESS => self::RUTHLESS,
        self::SOLE_SURVIVOR => self::SOLE_SURVIVOR,
        self::WAR_HERO => self::WAR_HERO,
    ];

    public function choose(): string
    {
        return array_rand($this->choices);
    }
}
