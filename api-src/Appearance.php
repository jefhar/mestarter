<?php

namespace MEApi;

class Appearance
{

    /**
     * @param string $gender
     * @return array
     * @throws \Exception
     */
    public function describe(string $gender): array
    {
        $description = [
            'Facial Structure' => $this->facialStructure($gender),
            'Head' => $this->head(),
            'Eyes' => $this->eyes($gender),
            'Jaw' => $this->jaw(),
            'Mouth' => $this->mouth($gender),
            'Nose' => $this->nose(),
            'Hair' => $this->hair($gender),
        ];

        if ($gender === 'female') {
            $description['Makeup'] = $this->makeup();
        }

        return $description;
    }

    /**
     * @param int $maxValue
     * @return array
     * @throws \Exception
     */
    private function value(int $maxValue): array
    {
        $maxValue = max(1, $maxValue);

        return ['value' => random_int(1, $maxValue), 'max' => $maxValue];
    }

    /**
     * @param string $gender
     * @return array
     * @throws \Exception
     */
    private function facialStructure(string $gender): array
    {
        $facial_structure = [
            'male' => 6,
            'female' => 9,
        ];
        $scar = [
            'male' => 13,
            'female' => 11,
        ];

        return [
            'Facial Structure' => $this->value($facial_structure[$gender]),
            'Skin Tone' => $this->value(6),
            'Complexion' => $this->value(3),
            'Scar' => $this->value($scar[$gender]),
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function head(): array
    {
        return [
            'Neck Thickness' => $this->value(31),
            'Face Size' => $this->value(31),
            'Cheek Width' => $this->value(31),
            'Cheek Bones' => $this->value(31),
            'Cheek Gaunt' => $this->value(31),
            'Ears Size' => $this->value(31),
            'Ears Orientation' => $this->value(31),
        ];
    }

    /**
     * @param string $gender
     * @return array
     * @throws \Exception
     */
    private function eyes(string $gender): array
    {
        $eye_shape = [
            'male' => 8,
            'female' => 9,
        ];

        return [
            'Eye Shape' => $this->value($eye_shape[$gender]),
            'Eye Height' => $this->value(31),
            'Eye Width' => $this->value(31),
            'Eye Depth' => $this->value(31),
            'Brow Depth' => $this->value(31),
            'Brow Height' => $this->value(31),
            'Iris Color' => $this->value(13),
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function jaw(): array
    {
        return [
            'Chin Height' => $this->value(31),
            'Chin Depth' => $this->value(31),
            'Chin Width' => $this->value(31),
            'Jaw Width' => $this->value(31),
        ];
    }

    /**
     * @param string $gender
     * @return array
     * @throws \Exception
     */
    private function mouth(string $gender): array
    {
        $mouthShape = ['male' => 9, 'female' => 10];

        return [
            'Mouth Shape' => $this->value($mouthShape[$gender]),
            'Mouth Depth' => $this->value(31),
            'Mouth Width' => $this->value(31),
            'Mouth Lip Size' => $this->value(31),
            'Mouth Height' => $this->value(31),
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function nose(): array
    {
        return [
            'Nose Shape' => $this->value(12),
            'Nose Height' => $this->value(31),
            'Nose Depth' => $this->value(31),
        ];
    }

    /**
     * @param string $gender
     * @return array
     * @throws \Exception
     */
    private function hair(string $gender): array
    {
        $hair = [
            'Hair Color' => $this->value(7),
        ];
        if ($gender === 'male') {
            $hair['Beard'] = $this->value(14);
            $hair['Brow'] = $this->value(7);
            $hair['Hair'] = $this->value(8);
            $hair['Facial Hair Color'] = $this->value(6);
        } else {
            $hair['Hair'] = $this->value(10);
            $hair['Brow'] = $this->value(16);
            $hair['Brow Color'] = $this->value(7);
        }

        return $hair;
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function makeup(): array
    {
        return [
            'Blush Color' => $this->value(6),
            'Lip Color' => $this->value(7),
            'Eye Shadow Color' => $this->value(7),
        ];
    }
}
