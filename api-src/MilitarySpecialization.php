<?php
declare (strict_types=1);

namespace MEApi;

use MEApi\BonusTalent\BonusTalent;

class MilitarySpecialization
{
    private const ADEPT = 'Adept';
    private const ENGINEER = 'Engineer';
    private const INFILTRATOR = 'Infiltrator';
    private const SENTINEL = 'Sentinel';
    private const SOLDIER = 'Soldier';
    private const VANGUARD = 'Vanguard';

    private $choices = [
        self::ADEPT => self::ADEPT,
        self::ENGINEER => self::ENGINEER,
        self::INFILTRATOR => self::INFILTRATOR,
        self::SENTINEL => self::SENTINEL,
        self::SOLDIER => self::SOLDIER,
        self::VANGUARD => self::VANGUARD,
    ];

    /** @var BonusTalent */
    private $BonusTalent;
    private $class;

    /**
     * @return string
     */
    public function choose(): string
    {
        $this->class = array_rand($this->choices);

        return $this->class;
    }

    /**
     * @return string
     */
    public function chooseBonusTalent(): string
    {
        $talentClass = 'MEApi\\BonusTalent\\' . $this->class;
        /** @var BonusTalent $bonusTalent */
        $bonusTalent = new $talentClass();

        return $bonusTalent->choose();
    }
}
