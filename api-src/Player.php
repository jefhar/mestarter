<?php
declare (strict_types=1);

namespace MEApi;

use Faker\Factory;
use Faker\Generator;
use VRia\Utils\NoDiacritic;

class Player
{
    const NO_RANDOM_AVAILABLE = 'Unable to generate a random number. Cannot proceed. Exiting';

    /**
     * @var Generator
     */
    private $faker;
    /**
     * @var Appearance
     */
    private $Appearance;
    /**
     * @var MilitarySpecialization
     */
    private $MilitarySpecialization;
    /**
     * @var PreserviceHistory
     */
    private $PreserviceHistory;
    /**
     * @var PsychologicalProfile
     */
    private $PsychologicalProfile;

    public function __construct()
    {
        $this->setupFaker();

        $this->MilitarySpecialization = new MilitarySpecialization();
        $this->PreserviceHistory = new PreserviceHistory();
        $this->PsychologicalProfile = new PsychologicalProfile();
        $this->Appearance = new Appearance();
    }

    /**
     * @return string
     */
    public function json(): string
    {
        $gender = $this->chooseGender();
        try {
            $player = [
                'gender' => ucfirst($gender),
                'name' => $this->chooseName($gender),
                'history' => $this->PreserviceHistory->choose(),
                'profile' => $this->PsychologicalProfile->choose(),
                'specialization' => $this->MilitarySpecialization->choose(),
                'bonusTalent' => $this->MilitarySpecialization->chooseBonusTalent(),
                'appearance' => $this->Appearance->describe($gender),
            ];
        } catch (\Exception $e) {
            die(self::NO_RANDOM_AVAILABLE);
        }

        return json_encode($player, JSON_THROW_ON_ERROR);
    }

    /**
     * @return string
     */
    private function chooseGender(): string
    {
        try {
            return random_int(0, 1) ? 'male' : 'female';
        } catch (\Exception $e) {
            die(self::NO_RANDOM_AVAILABLE);
        }
    }

    /**
     * @param string $gender
     * @return string
     */
    private function chooseName(string $gender): string
    {
        return NoDiacritic::filter($this->faker->firstName($gender));
    }

    private function setupFaker(): void
    {
        $faker = Factory::create();
        $faker->addProvider(new \Faker\Provider\cs_CZ\Person($faker));
        $faker->addProvider(new \Faker\Provider\da_DK\Person($faker));
        $faker->addProvider(new \Faker\Provider\de_AT\Person($faker));
        $faker->addProvider(new \Faker\Provider\de_CH\Person($faker));
        $faker->addProvider(new \Faker\Provider\de_DE\Person($faker));
        $faker->addProvider(new \Faker\Provider\el_GR\Person($faker));
        $faker->addProvider(new \Faker\Provider\en_GB\Person($faker));
        $faker->addProvider(new \Faker\Provider\en_IN\Person($faker));
        $faker->addProvider(new \Faker\Provider\en_NG\Person($faker));
        $faker->addProvider(new \Faker\Provider\en_UG\Person($faker));
        $faker->addProvider(new \Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new \Faker\Provider\en_ZA\Person($faker));
        $faker->addProvider(new \Faker\Provider\es_AR\Person($faker));
        $faker->addProvider(new \Faker\Provider\es_ES\Person($faker));
        $faker->addProvider(new \Faker\Provider\es_VE\Person($faker));
        $faker->addProvider(new \Faker\Provider\et_EE\Person($faker));
        $faker->addProvider(new \Faker\Provider\fi_FI\Person($faker));
        $faker->addProvider(new \Faker\Provider\fr_BE\Person($faker));
        $faker->addProvider(new \Faker\Provider\fr_CA\Person($faker));
        $faker->addProvider(new \Faker\Provider\fr_CH\Person($faker));
        $faker->addProvider(new \Faker\Provider\fr_FR\Person($faker));
        $faker->addProvider(new \Faker\Provider\hr_HR\Person($faker));
        $faker->addProvider(new \Faker\Provider\hu_HU\Person($faker));
        $faker->addProvider(new \Faker\Provider\id_ID\Person($faker));
        $faker->addProvider(new \Faker\Provider\is_IS\Person($faker));
        $faker->addProvider(new \Faker\Provider\it_CH\Person($faker));
        $faker->addProvider(new \Faker\Provider\it_IT\Person($faker));
        $faker->addProvider(new \Faker\Provider\lt_LT\Person($faker));
        $faker->addProvider(new \Faker\Provider\lv_LV\Person($faker));
        $faker->addProvider(new \Faker\Provider\me_ME\Person($faker));
        $faker->addProvider(new \Faker\Provider\ms_MY\Person($faker));
        $faker->addProvider(new \Faker\Provider\nb_NO\Person($faker));
        $faker->addProvider(new \Faker\Provider\ne_NP\Person($faker));
        $faker->addProvider(new \Faker\Provider\nl_BE\Person($faker));
        $faker->addProvider(new \Faker\Provider\nl_NL\Person($faker));
        $faker->addProvider(new \Faker\Provider\pl_PL\Person($faker));
        $faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
        $faker->addProvider(new \Faker\Provider\pt_PT\Person($faker));
        $faker->addProvider(new \Faker\Provider\ro_MD\Person($faker));
        $faker->addProvider(new \Faker\Provider\ro_RO\Person($faker));
        $faker->addProvider(new \Faker\Provider\sk_SK\Person($faker));
        $faker->addProvider(new \Faker\Provider\sl_SI\Person($faker));
        $faker->addProvider(new \Faker\Provider\sr_Latn_RS\Person($faker));
        $faker->addProvider(new \Faker\Provider\sv_SE\Person($faker));
        $faker->addProvider(new \Faker\Provider\tr_TR\Person($faker));

        $this->faker = $faker;
    }
}
