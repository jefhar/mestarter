<?php
declare (strict_types=1);

namespace MEApi\BonusTalent;

class Soldier extends Talent
{
    protected $choices = [
        self::DECRYPTION => self::DECRYPTION,
        self::LIFT => self::LIFT,
        self::SINGULARITY => self::SINGULARITY,
        self::THROW => self::THROW,
        self::WARP => self::WARP,
    ];
}
