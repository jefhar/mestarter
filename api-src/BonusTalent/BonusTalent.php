<?php
declare (strict_types=1);

namespace MEApi\BonusTalent;

interface BonusTalent
{
    public function choose(): string;
}
