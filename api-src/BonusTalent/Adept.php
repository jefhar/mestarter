<?php
declare (strict_types=1);

namespace MEApi\BonusTalent;

class Adept extends Talent
{
    protected $choices = [
        self::DECRYPTION => self::DECRYPTION,
        self::SHOTGUNS => self::SHOTGUNS,
    ];
}
