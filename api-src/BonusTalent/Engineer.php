<?php
declare (strict_types=1);

namespace MEApi\BonusTalent;

class Engineer extends Talent
{
    protected $choices = [
        self::LIFT => self::LIFT,
        self::SHOTGUNS => self::SHOTGUNS,
        self::SINGULARITY => self::SINGULARITY,
        self::THROW => self::THROW,
        self::WARP => self::WARP,
    ];
}
