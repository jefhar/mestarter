<?php
declare (strict_types=1);

namespace MEApi\BonusTalent;

class Vanguard extends Talent
{
    protected $choices = [
        self::DECRYPTION => self::DECRYPTION,
        self::SINGULARITY => self::SINGULARITY,
    ];
}
