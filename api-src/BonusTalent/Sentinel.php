<?php
declare (strict_types=1);

namespace MEApi\BonusTalent;

class Sentinel extends Talent
{
    protected $choices = [
        self::SHOTGUNS => self::SHOTGUNS,
        self::SINGULARITY => self::SINGULARITY,
        self::WARP => self::WARP,
    ];
}
