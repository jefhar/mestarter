<?php
declare (strict_types=1);

namespace MEApi\BonusTalent;

abstract class Talent
{
    protected const DECRYPTION = 'Decryption';
    protected const LIFT = 'Lift';
    protected const SINGULARITY = 'Singularity';
    protected const THROW = 'Throw';
    protected const WARP = 'Warp';
    protected const SHOTGUNS = 'Shotguns';
    protected $choices;

    public function choose(): string
    {
        return array_rand($this->choices);
    }
}
