<?php
declare (strict_types=1);
namespace MEApi;

class PreserviceHistory
{
    private const COLONIST = 'Colonist';
    private const EARTHBORN = 'Earthborn';
    private const SPACER = 'Spacer';

    private $choices = [
        self::COLONIST => self::COLONIST,
        self::EARTHBORN => self::EARTHBORN,
        self::SPACER => self::SPACER,
    ];

    public function choose(): string
    {
        return array_rand($this->choices);
    }
}
