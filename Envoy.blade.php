@servers(['web' => 'deploy@api-mestarter.jeffharris.us'])

@setup
    $release = date('YmdHis');
    $repository = 'git@gitlab.com:jefhar/mestarter.git';
    $app_dir = '/var/www/api-mestarter';
    $releases_dir = $app_dir . '/releases';
    $new_release_dir = $releases_dir . '/' . $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir -p {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    echo 'Running composer'
    cd {{ $new_release_dir }}
    composer install --no-dev --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask
