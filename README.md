This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Shepard Generator
Next time you start your Mass Effect trilogy play through, how do you start your
Shepard? Play as the same type of character you played last time, or play as the
same type of character you played last time with a little change?

The Mass Effect Shepard Generator creates a random Shepard for your next
playthrough. Just visit the site, and you get a new Shepard. Open the various
Appearance categories to build your Shephead.

Click the button at the bottom for a new Shepard.

## Behind the scenes
To generate a Shepard, this page makes an api call which generates random choices
to the selections. Each selection is enumerated by the game, except for Shepard's first name. The page then fills in the character profile and the Shephead details.

The api uses [Faker](https://github.com/fzaninotto/Faker) to generate the name. It takes all names from languages that use characters that can have any diacriticals removed to appear in the Latin character set.